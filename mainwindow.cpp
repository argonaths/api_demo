#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    D_m("DEBUG IS ON");

    numberConnector nc(NUMBER);
    this->connectors.push_back(nc);

    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_searchButton_clicked()
{
    D_m("Button Pressed");
    std::string input  = ui->input_bar->text().toStdString();
    std::vector <std::string> split = inputParser::split(input);
    for( size_t i = 0; i < split.size(); i++) {
        D_m(":"+split.at(i)+":");
    }
    std::vector<int> numbers;
    numbers = inputParser::find_int(&split);
    for (int &i : numbers) {
//        if(numberConnector* tmp_c = dynamic_cast<numberConnector*>(&connectors.at(0)) ) {
//            tmp_c->create_request(66);
//        }
        this->connectors.at(0).create_request(i);
        D_m(this->connectors.at(0).print_results());
    }
    D_m("done");
}
