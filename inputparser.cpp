#include "inputparser.h"
#include <iostream>
#include "debug.h"

namespace inputParser {

std::vector<std::string> split(std::string input) {
    std::vector <std::string> result_p;		// = new std::vector <std::string> ();
    size_t current, prevoius = 0;
    current = input.find(' ');

    while(current != std::string::npos) {
        std::string sub_string = input.substr(prevoius, current - prevoius);
        if (sub_string != "") {
            result_p.push_back(sub_string);
        }

        prevoius = current + 1;
        current = input.find(' ', prevoius);
    }
    result_p.push_back(input.substr(prevoius, current - prevoius));

    return result_p;
}


std::vector<int> find_int(std::vector<std::string>* split) {
    std::vector<int> result;	// = new std::vector<int>;
    int tmp;
    for (auto &s : *split) {
        try {
            tmp = std::stoi(s);
        } catch (std::invalid_argument& e) {
            D_m(std::string(e.what()) + ": \"" + s + "\" is not convertable to number");						// it works for "32t" -> 32
            continue;
        }
        result.push_back(tmp);
    }

    return result;
}


}
