#include "number_c.h"

numberConnector::numberConnector(connector_t type) : apiConnector(type){
    matching_str.push_back("trivia");
    matching_str.push_back("math");
    matching_str.push_back("date");

    base_url = "http://numbersapi.com/";

    for (size_t i = 0; i < REQUIREMENTS_COUNT; ++i) {
        req_arr[i] = false;
    }


}

numberConnector::~numberConnector() {

}

///////////////////////////////////////////////////////////////////////////////
//////   PRIVATE
///////////////////////////////////////////////////////////////////////////////

std::string numberConnector::translate_request_mode(request_mode rm) {
    if (rm == request_mode::DATE) {
        return "date";
    } else if (rm == request_mode::TRIVIA) {
        return "trivia";
    } else if (rm == request_mode::MATH) {
        return "math";
    } else {
        // TODO throw err
        return "unknown";
    }
}

///////////////////////////////////////////////////////////////////////////////
//////   PUBLIC
///////////////////////////////////////////////////////////////////////////////

void numberConnector::create_request(cpr::Url* url, cpr::Payload* payload, cpr::Header* header) {
    if ((payload == nullptr) || (header == nullptr)) {
        // return error
    }
    auto r = cpr::Get(url, payload, header);
    // TODO check errors
    resp = r;
}

void numberConnector::create_request(size_t argc[REQUIREMENTS_COUNT], void** argv) {
    // TODO :)
    if (argc == nullptr || argv == nullptr) {
        return;
    }

}

std::string numberConnector::print_results() {
    if (resp->status_code != 200) {
        return "Error ";			// TODO throw err
    }
    return resp->text;
}

std::string numberConnector::print_info() {
    return "This returns info about the connector";
}

void numberConnector::create_request() {
    //create_request for random number on default mode
    std::string final_url_s = "random/trivia";
    cpr::Url final_url(final_url_s);
    cpr::Response r = cpr::Get(final_url);
    // TODO check errors
    resp = r;
}

void numberConnector::create_request(int number) {
    std::string final_url_s = base_url+std::to_string(number)+"/trivia";	// TODO change 666 for variable number
    cpr::Url final_url(final_url_s);
    cpr::Response r = cpr::Get(final_url);
    // TODO check errors
    resp = r;
}

void numberConnector::create_request(int number, request_mode rm) {
    if ((rm != request_mode::TRIVIA) && (rm != request_mode::MATH)) {
        //paramater shoudl be of type date
        // TODO throw err
        return;
    }
    std::string final_url_s = base_url+std::to_string(number)+"/trivia";	// TODO change 666 for variable number, change api mode for variable
    cpr::Url final_url(final_url_s);
    cpr::Response r = cpr::Get(final_url);
    // TODO check results
    resp = r;
}

void numberConnector::create_request(std::string date) {
    std::string final_url_s = base_url+date+"/date";
    cpr::Url final_url(final_url_s);
    cpr::Response r = cpr::Get(final_url);
    // TODO check errors
    resp = r;
}

void numberConnector::create_request(int day, int month) {
    std::string date = std::to_string(month) + "/" +std::to_string(day);
    std::string final_url_s = base_url+date+"/date";
    cpr::Url final_url(final_url_s);
    cpr::Response r = cpr::Get(final_url);
    // TODO check errors
    resp = r;
}


