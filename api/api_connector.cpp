#include "api_connector.h"

apiConnector::apiConnector(connector_t type) {
    this->type = type;
}

apiConnector::~apiConnector() {

}

std::string apiConnector::print_info() {
    // Return information about connector in one string
    return return_type();
}

std::string apiConnector::return_type() {
    // Return with which API does the connector communicate
    switch (type) {
    case connector_t::NUMBER:
        return "NUMBER";
    case connector_t::LOTR:
        return "LotR";
    case connector_t::TWITTER:
        return "Twitter";
    }
    return "ERR";
}

int apiConnector::get_resp_err_code() {
    if (!resp) {
        printf("Object has not been initialized. Try creating request with create_request()");
        return -1;
    }
    return static_cast<int>(resp->error.code);
}

std::string apiConnector::get_resp_err_msg() {
    if (!resp) {
        printf("Object has not been initialized. Try creating request with create_request()");
        return "Object has not been initialized. Try creating request with create_request()";
    }
    return resp->error.message;
}

std::string apiConnector::get_resp_text() {
    if (!resp) {
        printf("Object has not been initialized. Try creating request with create_request()");
        return "Object has not been initialized. Try creating request with create_request()";
    }
    return resp->text;
}

std::string apiConnector::get_resp_url() {
    if (!resp) {
        printf("Object has not been initialized. Try creating request with create_request()");
        return "Object has not been initialized. Try creating request with create_request()";
    }
    return resp->url;
}

std::string apiConnector::get_resp_cookies_encoded() {
    if (!resp) {
        printf("Object has not been initialized. Try creating request with create_request()");
        return "Object has not been initialized. Try creating request with create_request()";
    }
    return resp->cookies.GetEncoded();
}

std::string apiConnector::get_resp_header() {
    //return resp.header;
    return "TODO";
}

int32_t apiConnector::get_resp_status_code() {
    if(!resp) {
        printf("Object has not been initialized. Try creating request with create_request()");
        return  -1;
    }
    return resp->status_code;
}
