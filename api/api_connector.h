#ifndef API_CONNECTOR_H
#define API_CONNECTOR_H

#include <string>
#include <optional>
#include <vector>

#include <cpr/cpr.h>

const size_t REQUIREMENTS_COUNT = 4;// Describes how many requirement types connector can have

enum connector_t {
    NUMBER,
    LOTR,
    TWITTER
};



class apiConnector {
public:
    enum class requirements {
        STRING,
        INT,
        DATE,				// Request date in normal format DD.MM.YYYY
        MATCH,			// Request matching strings specified in matching_str vector
    };

    apiConnector(connector_t type);
    virtual ~apiConnector();
    // Pure virtual functions
    virtual void create_request(cpr::Url* url, cpr::Payload*  payload, cpr::Header* header) = 0;
    virtual void create_request(size_t argc[REQUIREMENTS_COUNT], void** argv) = 0;
    virtual std::string print_results() = 0;
    // GET and SET
    std::string virtual print_info();
    std::string return_type();

    int get_resp_err_code();
    std::string get_resp_err_msg();
    std::string get_resp_text();
    std::string get_resp_url();
    std::string get_resp_cookies_encoded();
    std::string get_resp_header();
    int32_t get_resp_status_code();

protected:
    connector_t type;
    cpr::Url base_url;
    bool req_arr[REQUIREMENTS_COUNT];
    std::optional<cpr::Response> resp;
    std::vector<std::string> matching_str;
};

#endif// API_CONNECTOR_H
