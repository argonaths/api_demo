#ifndef NUMBERCONNECTOR_H
#define NUMBERCONNECTOR_H

#include "api_connector.h"


class numberConnector : public apiConnector {
public:
    enum class request_mode {
        DATE,
        TRIVIA,
        MATH
    };
    numberConnector(connector_t type);
    ~numberConnector() override;
    // Pure virtual functions
    void create_request(cpr::Url* url, cpr::Payload* payload, cpr::Header* header) override;
    void create_request(size_t argc[REQUIREMENTS_COUNT], void** argv) override;
    std::string print_results() override;

    std::string print_info() override;
    void create_request();
    void create_request(int number);
    void create_request(int number, request_mode rm);
    void create_request(std::string date);
    void create_request(int day, int month);

protected:


private:
    std::string virtual translate_request_mode(request_mode rt);

};

#endif// NUMBERCONNECTOR_H
