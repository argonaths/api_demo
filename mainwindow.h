#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore>

#include <cpr/cpr.h>
#include <iostream>

#include "api/number_c.h"
#include "debug.h"
#include "inputparser.h"


namespace Ui {
class MainWindow;
}

#define API_N 3

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_searchButton_clicked();

private:
    std::vector<numberConnector> connectors;
    Ui::MainWindow *ui;
};

#endif// MAINWINDOW_H
