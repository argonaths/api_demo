#ifndef INPUTPARSER_H
#define INPUTPARSER_H

#include <string>
#include <vector>

namespace inputParser {
std::vector<std::string> split(std::string input);
std::vector<int> find_int(std::vector<std::string>* split);
}

#endif// INPUTPARSER_H
