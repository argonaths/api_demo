#define DEBUG


#ifndef DEBUG_H
#define DEBUG_H

#ifdef DEBUG

#define D_m(x) (std::cout << (x) << std::endl)
#define D_e(x) (std::cerr << (x) << std::endl)
#else
#define D_m(x)
#define D_e(x)

#endif// DEBUG

#endif// DEBUG_H
